﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Valyuta {
    public partial class Form1 : Form {

        private string currentWindow = null;
        private Rate rate;
        private DataEnter dataEnter;
        private Report report;
        
        public Form1() {
            InitializeComponent();
        }

        private void курсToolStripMenuItem_Click(object sender, EventArgs e) {
            currentWindow = "course";
            rate = new Rate(dgView);
        }

        private void вводДанныхToolStripMenuItem_Click(object sender, EventArgs e) {
            currentWindow = "DataEnter";
            dataEnter = new DataEnter(dgView);
        }

        private void отчетToolStripMenuItem_Click(object sender, EventArgs e) {
            currentWindow = "report";
            report = new Report(dgView);
        }

        private void btnAdd_Click(object sender, EventArgs e) {
            switch (currentWindow) {
                case "report":
                    dgView.Rows.Add("1", "2", "3", "4");
                    break;
                case "DataEnter":
                    dgView.Rows.Add("1", "2", "3", "4");
                    break;
                case "course":
                    rate.getAllRates();
                    //dgView.DataSource = rate.getAllRates();
                    break;
            }
        }

        private void btnRemove_Click(object sender, EventArgs e) {
            dgView.Rows.Add("", "");
        }

        private void btnSave_Click(object sender, EventArgs e) {
            //dgView.Rows.Add("");
            Connection.TestMethod();
        }
    }
}
