﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Data;

namespace Valyuta {

    class Rate {
        public DataGridViewTextBoxColumn id = new DataGridViewTextBoxColumn();
        public DataGridViewTextBoxColumn currencyCode = new DataGridViewTextBoxColumn();
        public DataGridViewTextBoxColumn currencyRate = new DataGridViewTextBoxColumn();
        public DataGridViewTextBoxColumn currencyName = new DataGridViewTextBoxColumn();
        public SqlCeCommand command = new SqlCeCommand();
        public Connection connection = new Connection();

        public Rate(DataGridView dg) {
            command.Connection = connection.opened_connection();

            dg.Columns.Clear();

            dg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.currencyRate,
            this.currencyCode,
            this.currencyName
            });
        }

        public void getAllRates() {
            command.CommandText = "select * from currency";
            SqlCeDataReader reader = command.ExecuteReader();
            while (reader.Read()) { 

            }
        }

        public void saveRates(string code, string name, double rate){
            command.CommandText = "insert into currency(code, name, rate) values (@code, @name, @rate)";
            command.Parameters.Add("@code", code);
            command.Parameters.Add("@name", name);
            command.Parameters.Add("@rate", rate);
            command.ExecuteNonQuery();
        }

        public void removeCurrencies() {
            command.CommandText = "delete from currency";
            command.ExecuteNonQuery();
        }
    }
}
