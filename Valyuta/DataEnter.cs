﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Valyuta {

    class DataEnter {

        public DataGridViewTextBoxColumn id = new DataGridViewTextBoxColumn();
        public DataGridViewTextBoxColumn user = new DataGridViewTextBoxColumn();
        public DataGridViewTextBoxColumn rate = new DataGridViewTextBoxColumn();
        public DataGridViewTextBoxColumn sum = new DataGridViewTextBoxColumn();

        public DataEnter(DataGridView dg) {
            dg.Columns.Clear();
            dg.Columns.AddRange(new DataGridViewColumn[]{
                this.id, 
                this.user, 
                this.rate, 
                this.sum
            });
        }
    }
}
