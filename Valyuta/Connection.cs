﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlServerCe;
using System.Configuration;

namespace Valyuta {
    class Connection {
        string connectionString = "Data Source=valyuta.sdf;Password=tajikistan";
        SqlCeConnection connection = new SqlCeConnection();

        public Connection() {
            connection.ConnectionString = connectionString;
        }

        
        public SqlCeConnection opened_connection() {
            if (connection.State != System.Data.ConnectionState.Open) {
                connection.Open();
            } else {
                connection.Close();
                connection.Open();
            }
            return this.connection;
        }


        public void close_connection() {
            this.connection.Close();
        }

        public static void TestMethod() {
            valyutaEntities4 db = new valyutaEntities4();
            operation oper = db.operations.SingleOrDefault(p => p.id == 1);
            oper.comment = "helloworld";
            int a = db.SaveChanges();
            Console.WriteLine(a.ToString());
        }
    }
}
